/**
 * pson
 *
 * Make json5 simple and powerful
 */
const regMultiline = /\B`([\s\S]+?[^\\])`/g
const regKeyValue = /^\s*([^=]+)\s*=\s*(.*)?\s*$/
const booleanValue = ['true', 'false']
const commentChar = ['//', '#']

function multilineReplacer (match, p1) {
  return p1.replace(/\n/g, '\\n')
}

function parseKey(value) {
  return value.trim()
}

function parseValue(value) {
  if (booleanValue.includes(value)) {
    // Boolean
    return value === 'true'
  } else if (/^\d+\.?\d*$/.test(value)) {
    // Number
    return Number(value)
  } else if (value.startsWith('[') && value.endsWith(']')) {
    // Array
    return value
      .slice(1, -1)
      .trim()
      .split(', ')
      .map(parseValue)
  }

  // String
  return value
    // expand newlines in quoted values
    .replace(/\\n/g, '\n')
    // remove any surrounding quotes
    .replace(/(^['"]|['"]$)/g, '')
    // remove extra spaces
    .trim()
}

function parse(str) {
  const obj = {}
  let prevKey = ''

  str
    .trim()
    .replace(regMultiline, multilineReplacer)
    .split(/\n+/)
    .forEach((line) => {
      if (!commentChar.includes(line[0])) {
        if (!line.includes('=') && prevKey) {
          obj[prevKey] = obj[prevKey] + '\n' + line.trim()
          return
        }

        const keyValue = line.match(regKeyValue)

        if (!keyValue) return

        const key = keyValue[1]
        prevKey = parseKey(key)

        // default undefined or missing values to empty string
        const value = keyValue[2] || ''

        obj[prevKey] = parseValue(value)
      }
    })

  return obj
}

const PSON = { parse }

module.exports = PSON
