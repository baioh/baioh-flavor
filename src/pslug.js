/**
 * pslug
 *
 * used to slugify the page name
 *
 * trim
 * remove '/', '\'
 * replace ' ' to '-'
 * lowercase
 */

const regReplace = /\s+/g
const regRemove = /(\\|\/)/g

module.exports = function(str) {
  return str
    .trim()
    .replace(regRemove, '')
    .replace(regReplace, '-')
    .toLowerCase()
}
